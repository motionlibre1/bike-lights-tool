// motionlibre bike-lights-tool is licensed under the GNU General Public License v3.0
// Put together by Cornel-Florentin Dimitriu, in the motionlibre project

//Inspired by / code used from the following projects:

/* Pieter P
   https://tttapa.github.io/ESP8266/Chap10%20-%20Simple%20Web%20Server.html
   https://github.com/tttapa/ESP8266
   tttapa/ESP8266 is licensed under the GNU General Public License v3.0 */

/* ESP8266 BlinkWithoutDelay by Simon Peter
   Blink the blue LED on the ESP-01 module
   Based on the Arduino Blink without Delay example
   This example code is in the public domain */


#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h> 
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>

ESP8266WiFiMulti wifiMulti;     // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'

ESP8266WebServer server(80);    // Create a webserver object that listens for HTTP request on port 80

// Variables to be set by user

const int left_led = 12;        // Setting the pin number for the left LED
const int right_led = 13;       // Setting the pin number for the right LED
const long interval = 500;      // Setting the blinking interval

const long baudrate = 115200;   // Setting the baud rate of the serial interface

// Initial values

int left_ledOn = 0;             // Setting the initial value for the left LED flag
int right_ledOn = 0;            // Setting the initial value for the right LED flag
int switch_mode = 0;            // Setting the initial value for the mode-switching flag

int right_ledState = LOW;            // Setting the initial value for the right LED state
int left_ledState = LOW;             // Setting the initial value for the right LED state
unsigned long previousMillis = 0;    // Setting the initial value for the millis counter

void handleRoot();              // function prototypes for HTTP handlers
void right_LED_on();            // Enable the right LED
void left_LED_on();             // Enable the left LED
void both_LEDs_on();            // Enable both LEDs
void both_LEDs_off();           // Turn both LEDs off
void handleNotFound();

void setup(void){
  Serial.begin(baudrate);         // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println('\n');

  pinMode(left_led, OUTPUT);     // Set the pin for the left LED as output pin
  pinMode(right_led, OUTPUT);    // Set the pin for the right LED as output pin


// Start of "Start an AP" section

const char* ssid = "bike lights tool";
const char* password = "developer";

 WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

// End of "Start an AP" section

// If you want to connect to an existing AP instead (or more), then uncomment to following, and comment the AP section above
/*

  //wifiMulti.addAP("ssid_from_AP_1", "your_password_for_AP_1");   // add Wi-Fi networks you want to connect to
  //wifiMulti.addAP("ssid_from_AP_2", "your_password_for_AP_2");
  //wifiMulti.addAP("ssid_from_AP_3", "your_password_for_AP_3");

  Serial.println("Connecting ...");
  int i = 0;
  while (wifiMulti.run() != WL_CONNECTED) { //  for the Wi-Fi to connect: scan for Wi-Fi networks, and connect to the strongest of the networks above
    delay(250);
    Serial.print('.');
  }
  Serial.println('\n');
  Serial.print("Connected to ");
  Serial.println(WiFi.SSID());              // Tell us what network we're connected to
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());           // Send the IP address of the ESP8266 to the computer

  if (MDNS.begin("esp8266")) {              // Start the mDNS responder for esp8266.local
    Serial.println("mDNS responder started");
  } else {
    Serial.println("Error setting up MDNS responder!");
  }

*/

  server.on("/", HTTP_GET, handleRoot);     // Call the 'handleRoot' function when a client requests URI "/"
  server.on("/right_LED_on", HTTP_POST, right_LED_on);  // Call the 'right_LED_on' function when a POST request is made to URI "/LED"
  server.on("/left_LED_on", HTTP_POST, left_LED_on);  // Call the 'left_LED_on' function when a POST request is made to URI "/LED"
  server.on("/both_LEDs_on", HTTP_POST, both_LEDs_on);  // Call the 'both_LEDs_on' function when a POST request is made to URI "/LED"
  server.on("/both_LEDs_off", HTTP_POST, both_LEDs_off);  // Call the 'both_LEDs_off' function when a POST request is made to URI "/LED"

  server.onNotFound(handleNotFound);        // When a client requests an unknown URI (i.e. something other than "/"), call function "handleNotFound"

  server.begin();                           // Actually start the server
  Serial.println("HTTP server started");

// Check the LEDs

  digitalWrite(left_led, HIGH);
  digitalWrite(right_led, HIGH);
  delay(50);
  digitalWrite(left_led, LOW);
  digitalWrite(right_led, LOW);
  delay(100);
    digitalWrite(left_led, HIGH);
  digitalWrite(right_led, HIGH);
  delay(50);
  digitalWrite(left_led, LOW);
  digitalWrite(right_led, LOW);
}

void loop(void){

  unsigned long currentMillis = millis();

if (switch_mode == 1) {             // If the mode should be switched, don't wait until the interval is finished
  left_ledState = LOW;              // Turn both LEDs off first
  right_ledState = LOW;
  switch_mode = 0;                  // Confirm the switch can be done the next run of the loop
} else
{

if (currentMillis - previousMillis >= interval) {       // If the interval has been reached
  previousMillis = currentMillis;    // save the last time you blinked the LED

  if (right_ledOn == 1) {            // If the right LED should blink
    if (right_ledState == LOW) {     // And it's off
      right_ledState = HIGH;         // turn it on
    } else {
      right_ledState = LOW;          // Otherwise, turn it off
    }
  } else {
    right_ledState = LOW;            // If it shouldn't blink, then just turn it off
  }
     
if (left_ledOn == 1) {               // If the left LED should blink
        if (left_ledState == LOW) {  // And it's off
      left_ledState = HIGH;          // turn it on
    } else {
      left_ledState = LOW;           // Otherwise, turn it off
    }
  } else {
    left_ledState = LOW;            // If it shouldn't blink, then just turn it off
  }
}
}

    digitalWrite(left_led, left_ledState);      // set the left LED with the ledState of the variable
    digitalWrite(right_led, right_ledState);    // set the right LED with the ledState of the variable

  server.handleClient();                    // Listen for HTTP requests from clients
}

void handleRoot() {                         // When URI / is requested, send a web page with buttons to toggle the LEDs
  server.send(200, "text/html", "\
  <meta charset=\"UTF-8\">\
  <table style=\"width:100%;\">\
      <tr>\
          <td colspan=\"3\" style=\"font-family:Tahoma;sans-serif; font-size: 8em;\">Bike Lights Tool</td>\
      </tr>\
      <tr>\
          <td></td>\
          <td>\
              <form action=\"/both_LEDs_on\" method=\"POST\">\
                  <input type=\"submit\" style=\"font-size: 8em; margin-top:0.3em;\"value=\"⚠\">\
              </form>\
          </td>\
          <td></td>\
      </tr>\
      <tr>\
          <td>\
              <form action=\"/left_LED_on\" method=\"POST\"><input type=\"submit\" style=\"font-size: 8em; margin-top:0.3em;\"value=\"⇦\">\
              </form>\
          </td>\
          <td>\
              <form action=\"/both_LEDs_off\" method=\"POST\"><input type=\"submit\" style=\"font-size: 8em; margin-top:0.3em;\"value=\"✕\">\
              </form>\
          </td>\
          <td>\
              <form action=\"/right_LED_on\" method=\"POST\"><input type=\"submit\" style=\"font-size: 8em; margin-top:0.3em;\"value=\"⇨\">\
              </form>\
          </td>\
      </tr>\
  </table>");
}


void right_LED_on() {                       // If a POST request is made to URI /LED
  right_ledOn = 1;                          // The right LED should be enabled
  left_ledOn = 0;                           // The left LED should be disabled
  switch_mode = 1;                          // Flag to switch modes
  server.sendHeader("Location","/");        // Add a header to respond with a new location for the browser to go to the home page again
  server.send(303);                         // Send it back to the browser with an HTTP status 303 (See Other) to redirect
}

void left_LED_on() {                        // If a POST request is made to URI /LED
  right_ledOn = 0;                          // The right LED should be disabled
  left_ledOn = 1;                           // The left LED should be enabled
  switch_mode = 1;                          // Flag to switch modes
  server.sendHeader("Location","/");        // Add a header to respond with a new location for the browser to go to the home page again
  server.send(303);                         // Send it back to the browser with an HTTP status 303 (See Other) to redirect
}

void both_LEDs_on() {                       // If a POST request is made to URI /LED
  right_ledOn = 1;                          // The right LED should be enabled
  left_ledOn = 1;                           // The left LED should be enabled
  switch_mode = 1;                          // Flag to switch modes
  server.sendHeader("Location","/");        // Add a header to respond with a new location for the browser to go to the home page again
  server.send(303);                         // Send it back to the browser with an HTTP status 303 (See Other) to redirect
}

void both_LEDs_off() {                      // If a POST request is made to URI /LED
  right_ledOn = 0;                          // The right LED should be disabled
  left_ledOn = 0;                           // The left LED should be disabled
  switch_mode = 1;                          // Flag to switch modes
  server.sendHeader("Location","/");        // Add a header to respond with a new location for the browser to go to the home page again
  server.send(303);                         // Send it back to the browser with an HTTP status 303 (See Other) to redirect
}

void handleNotFound(){
  server.send(404, "text/plain", "404: Not found"); // Send HTTP status 404 (Not Found) when there's no handler for the URI in the request
}
